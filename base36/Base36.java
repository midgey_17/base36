/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base36;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author John Mitchell
 * @email midgey17@hotmail.com
 * 
 * 
 * Change either of the two variables at the beginning; base10Num, base36Num
 * 
 * The method convertToBase36 takes the base10Num long and converts it to a base36 String
 * The method convertToBase10 takes the base36Num String and converts it to a base10 number
 */
public class Base36 extends Application {    
    /**
     * @param args the command line arguments
     */                    
            
    //TEXT + LABELS
    Text warningText = new Text("Max Value is: 1Y2P0IJ32E8E6");
    Text resultText = new Text("Result"); 
    Label titleLabel = new Label("Enter AlphaNumeric Value");       
        
    
    public static void main(String[] args) {                                                
        // declare variables
        long base10Num = Long.MAX_VALUE-1; //change this value
        String base36Num = "ZIK0ZH"; //change this value
        
        Number b10Number = new Number();
        Number b36Number = new Number();
        
        Number nextB10Number = new Number();
        Number nextB36Number = new Number();
                                
        //***** Simple test of the method convertToBase36 *****
        System.out.println("********** Base36 Conversion **********");        
        System.out.println(base10Num+" Converted to Base36 = "+b10Number.convertToBase36(base10Num));
        nextB10Number.setValueWithBase10(base10Num+1);
        System.out.println("Next is: "+nextB10Number.base10Value+" Converted to Base36 = "+nextB10Number.base36Value);
        //System.out.println(base10Num+" Converted to base36 = "+convertToBase36(base10Num));  
                
        //***** Simple test of the method convertToBase10 *****
        System.out.println();
        System.out.println("********** Base10 Conversion **********");
        System.out.println(base36Num+" Converted to Base10 = "+b36Number.convertToBase10(base36Num));
        nextB36Number.setValueWithBase10(b36Number.base10Value+1);  
        System.out.println("Next is: "+nextB36Number.base36Value+" Converted to Base10 = "+nextB36Number.base10Value);
        //System.out.println(base36Num+" Converted to Base10 = "+convertToBase10(base36Num));
        //System.out.println("Grand total is: "+convertToBase10(base36Num));
        
        launch(args);
    } //main
    
    
    //*************************************************
    //************************************************* 
    //*************************************************
    
    //GUI
    @Override
    public void start(Stage primaryStage) 
    {
        primaryStage.setTitle("Base36 Calculator");        
        
        //PANES
        GridPane rootPane = new GridPane();
        GridPane topPane = new GridPane();
        BorderPane bottomPane = new BorderPane();
        rootPane.setAlignment(Pos.CENTER); 
        rootPane.setVgap(20);        
        topPane.setAlignment(Pos.CENTER);
        topPane.setVgap(5);                
        
        //USER INPUT FIELDS
        TextField inputField = new TextField();
        inputField.textProperty().addListener((ov, oldValue, newValue) -> {
        inputField.setText(newValue.toUpperCase());
        });
        
        //BUTTONS
        Button convertButton = new Button("Get Next Value");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.CENTER);
        HBox.setHgrow(convertButton, Priority.ALWAYS);
        convertButton.setMaxWidth(160);
        hbBtn.getChildren().add(convertButton);
        
        //ACTIONS
        convertButton.setOnAction((ActionEvent e) -> {
            executeInput(inputField.getText());
        } //handle
        );
        
        inputField.setOnKeyPressed((KeyEvent keyEvent) -> {
            if (keyEvent.getCode() == KeyCode.ENTER)  {  
                executeInput(inputField.getText());
            } //if
        } //handle
        );
        
        //ADD OBJECTS TO PANES
        topPane.add(warningText, 0, 0);
        topPane.add(titleLabel, 0, 2);
        topPane.add(inputField, 0, 3);
        topPane.add(hbBtn, 0, 4);        
        bottomPane.setCenter(resultText);
        rootPane.add(topPane, 0, 0);
        rootPane.add(bottomPane, 0, 1);
        
        //SCENE
        Scene scene = new Scene(rootPane, 450, 200);
        primaryStage.setScene(scene);
        scene.getStylesheets().add
        (Base36.class.getResource("base36.css").toExternalForm());
        primaryStage.setMinWidth(450);
        primaryStage.setMinHeight(200);
        primaryStage.show();
        //primaryStage.close(); //uncomment this line to not use the GUI
        
        //ASSIGN CSS IDs
        resultText.setId("result-text"); 
        warningText.setId("warning-text"); 
        
    } //start(Stage)        
    
    public void executeInput(String input)
    {
        String part1, part2; //create two variables for the result string
        Number inputNumber = new Number(); //create a Number Object
        inputNumber.setValueWithBase36(input); //set the Number values with the user input
        part1 = input+" converted to base10 = "+inputNumber.base10Value+"\n"; //first part of result
        if(inputNumber.base10Value < Long.MAX_VALUE) { //check to make sure that the max value hasn't been reached
            inputNumber.setValueWithBase10(inputNumber.base10Value+1); //increase the value by 1
            part2 = "The next base36 value is "+inputNumber.base36Value; //second part of result
        } //if
        else { //don't attempt to increase value beyond max limit
            part2 = "Maximum value reached";
        } //else
        
        resultText.setText(part1+part2); //update the text at the bottom of the GUI
    } //executeInput
    
} //class
